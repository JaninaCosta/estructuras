﻿/*
concesionarioVersoin_14.0
1.extaje las funciones de janina de los menus 
2.cree el TDA Concecionario (es una Lista por lo que se puede usar las 
  funciones de List* pero ademas de las creadas, tal como Queue y Stack)
3.todo lo de TDA Concesionario dera exportada despues a Libreria.lib 
4.concecionario modifica un cliente
5.mejorardo la interfaz terminado modificar cliente
6.completo el submenu Clientes
7.TDA lista de vehiculos
8.Cmpleto submenu Vehiculos agregar/modificar/eliminar/>falta taller<
9.Implementado TDA's Mantenimiento y Taller para <op>Taller
10.Ambiente desde archivos Funcion "generar" genera clientes/vehiculos
11.Reportes opciones a,b,c con funcion "reporteVehiculos"
12.Rportes con HTML y CSS para opciones a) b) c)
13.Generar Abmbiente completo "generar"remplazada, funciones "generarClientesVheiculos"
   "generarTallerAtendidos"
14.0 Genera todos los Reportes COMPLETE* */

//LIBRERIAS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "../Libreria/concesionario.h"
#include "../Libreria/taller.h"
//CONSTANTES
#define NP 4 //Numero de Prioridades(tipos)



//PROTOTIPOS
void menuPrincipal();
void submenu(char *cadena);
void limpiar();

void reporteVehiculos(List *lista_vehiculos, Cliente *clie, char *cadena, int op);
void reporteMantenimientos(List *atendidos, Taller *taller, int op);


Concesionario *concesionario;// afuera porque la lista concesionario es como una cosatante unica del sistema

//FUNCION PRINCIPAL
void main(){
	srand(time(NULL));

	concesionario = concesionarioNew();

	int opcion = 0, tmp = 0, id = 0;
	char nombre[20], apellido[20], identificacion[10];
	int op1 = 0, op2 = 0, op3 = 0, idvehiculo, opId = 0, flag = 0;
	
	char marca[20], modelo[20], fecha_fabrica[20];
	int  estado, opTipo = 0;
	char tipovehiculo[20];

	//ingreso taller
	char diaSemana[10];
	float costo = 0;
	int km = 0, prio = 0;

	NodeList *nodo = NULL, *p = NULL, *q = NULL;
	Cliente *cliente = NULL;
	List *atendidos = listNew(), *clientes = listNew();
	Vehiculo *vehiculo = NULL;
	listaVehiculos *vehiculos = NULL;
	Taller *taller = tallerNew( NP);//inicializa con 4 prioridades
	Mantenimiento *m = NULL;
	FILE *p_fclientes = NULL, *p_ftaller = NULL, *p_fvehis = NULL, *p_fmant = NULL;

	do{
		limpiar();
		menuPrincipal();//lista menu principal
		scanf("%d", &op1); getchar();
		switch (op1)
		{
		case 1:
			//Generar Ambiente
			p_ftaller = fopen("Archivos/taller.txt", "r");
			p_fclientes = fopen("Archivos/clientes.txt", "r");
			p_fvehis = fopen("Archivos/vehiculos.txt", "r");
			p_fmant = fopen("Archivos/mantenimientos.txt", "r");
			if (p_fclientes == NULL || p_fvehis == NULL){
				printf("\nArchivo generador no encontrado"); getchar(); continue;
			}
			else{
				generarClientesVehiculos(p_fclientes, p_fvehis, concesionario);
				fclose(p_fclientes);
				generarTallerAtendidos(p_fmant, taller, atendidos);
				concesionarioListarCli(concesionario);
				printf("\n Ambiente generado con exito"); getchar();
			}
			fclose(p_ftaller);
			fclose(p_fvehis);
			fclose(p_fmant);
			break;
		case 2:
			//Clientes
 			do{
				limpiar();
				submenu("Clientes");
				scanf("%d", &opcion); getchar();
				limpiar();
				switch (opcion)
				{
				case 1: //Agreagar Cliente
					printf("\n\n------>Nuevo Cliente------\n");
					printf("\n\nNombre: "); scanf("%s", &nombre); 
					printf("\nApellido: "); scanf("%s", &apellido);
					printf("\nIdentificacion: "); scanf("%s", &identificacion); getchar();
					cliente = clienteNew( nombre, apellido, identificacion);
					nodo = nodeListNew(cliente);
					concesionarioAgregarCli(concesionario, cliente);

					printf("\n\t\t\t"); printCliente(cliente);
					printf("\n\t\t-------Cliente creado con exito!-------\n\n");
					printf("\nPresiones enter para volver.. "); getchar();

					break;
				case 2: //Modificar Cliente
					printf("\n\n-------Modificar Clientes-----\n\n");
					concesionarioListarCli(concesionario);

					printf("\nIngrese idetificacion del cliente a modificar: ");
					scanf("%s", &identificacion); getchar();
					
					if (concesionarioModificarCli(concesionario, identificacion) == 1 )
						printf("\n\t\t-------Cliente modificado con exito!-------\n\n");

					printf("\nPresiones enter para volver.. "); getchar();
					break;
				case 3: //Eliminar Cliente
					printf("\n\n-------Eliminar Clientes-----\n\n");
					concesionarioListarCli(concesionario);

					printf("\nIngrese idetificacion del cliente a eliminar: ");
					scanf("%s", &identificacion); getchar();
					if (concesionarioEliminarCli(concesionario, identificacion) == 1){
						limpiar();
						printf("\n\n-------Eliminar Clientes-----\n\n");
						printf("-------Cliente eliminado con exito!-------\n");
						concesionarioListarCli(concesionario);
					}

					printf("\nPressione enter pavara volver.. ");  getchar();
					break;
				case 4: // Listar Clientes
					concesionarioListarCli(concesionario);
					printf("\nPressione enter pavara volver.. ");  getchar();
					break;
				//case 5: salir del submenu clientes ya lo controla el while
				}
			} while (opcion != 5);
			break;
		case 3:
			//*******Vehiculos*******/
			limpiar();
			flag = 0;
			printf("\n------>SubMenu Vehiculos------\n");
			concesionarioListarCli(concesionario);
			
			printf("\nPara aceder acceder a la lista de vehiculos");
			printf("\nIngrese identificaion de un Cliente: ");
			scanf("%s", &identificacion); getchar();
			for (p = concesionario->header; p != NULL; p = p->next){
				cliente = nodeListGetCont(p);  //cliente para acceder a la lista de vehiculos

				if (strcmp(cliente->identificacion, identificacion) == 0){ flag = 1; break; }

			}
			if (flag == 0){ printf("\nCliente no encontrado..."); getchar(); continue; }
			do{
				limpiar();
				//submenu( strcat("Vehiculos",cliente->apellido) );			/-------> esta linea me da error de esta forma
				submenu("Vehiculos ");
				scanf("%d", &opcion); //getchar();
				limpiar();
				switch (opcion)
				{
				case 1: //Ingresar Vehiculo
					printf("\n\n------>Nuevo Vehiculo del Cliente %s------\n", cliente->apellido);
					printf("idvehiculo: "); scanf("%d", &idvehiculo); getchar();
					printf("Marca: "); scanf("%s", &marca); getchar();
					printf("Modelo: "); scanf("%s", &modelo); getchar();
					printf("Fecha_fabrica: "); scanf("%s", &fecha_fabrica); getchar();
					printf("Tipo: ");
						printf("\n\t1. Taxi\n");
						printf("\t2. Gama Alta\n");
						printf("\t3. Gama Media\n");
						printf("\t4. Gama Baja\n");
						printf("\nEscoja el tipo de vehiculo: ");
						scanf("%d", &opTipo); getchar();
						setTipo(opTipo, tipovehiculo);
					printf("Estado: ");
					scanf("%d", &estado); getchar();		

					vehiculo = vehiculoNew(idvehiculo, marca, modelo, fecha_fabrica, tipovehiculo, estado);
					listaVehiculosAgregar(cliente->vehiculos, vehiculo);
					
					printf("\n\t\t"); vehiculoPrint(vehiculo);
					printf("\n\t\t-------Vehiculo agregago con exito!-------\n\n");
					printf("\nPresiones enter para volver.. "); getchar();

					//free(vehiculo);
					break;
				case 2: //Modificar Vehiculo
					
					printf("\n\n-------Modificar Vehiculos del Cliente-----\n\n");

					listaVehiculosListar(cliente->vehiculos, cliente->apellido);
					if (listIsEmpty(cliente->vehiculos) == 1) { getchar(); continue; }
					printf("\nIngrese el id del vehiculo a modificar: ");
					scanf("%d", &opId); getchar();

					if (listaVehiculosModificar(cliente->vehiculos, opId) == 1){
						printf("\n\t\t-------Vehiculo modificado con exito!-------\n\n");
					}
					listaVehiculosListar(cliente->vehiculos, cliente->apellido);
					getchar();
					printf("\nPresiones enter para volver.. "); getchar();
					break;
				case 3: //Eliminar Vehiculo
					printf("\n\n-------Eliminar Vehiculo del Cliente-----\n\n");
					listaVehiculosListar(cliente->vehiculos, cliente->apellido);
					if (listIsEmpty(cliente->vehiculos) == 1) { getchar(); continue; }
					printf("\nIngrese el id del vehiculo a eliminar: ");
					scanf("%d", &opId); getchar();
					
					if (listaVehiculosEliminar(cliente->vehiculos, opId) == 1){ //1 si 
						limpiar();
						printf("\n\n-------Eliminar Vehiculo del Cliente-----\n\n");
						printf("-------Vehiculo eliminado con exito!-------\n");
						listaVehiculosListar(cliente->vehiculos, cliente->apellido);
					}
					printf("\nPressione enter pavara volver.. ");  getchar();
					break;
				case 4: // Taller -->lista de prioridad
					do{
						vehiculo = NULL;
						limpiar();
						printf("\n------Taller------\n");
						printf(" 1. Ingresar\n 2. Atender\n 3. Finalizar \n 4 Print Taller\n     Igrese opcion: ");
						scanf("%d", &op3); getchar();
						switch (op3)
						{
						case 1:// Ingresar al taller
							limpiar();
							printf("\n\n-------Ingreso al Taller-----\n\n");
							listaVehiculosListar(cliente->vehiculos, cliente->apellido);  //listo los vehiculos de ese cliente para elegur cual va a entrar al taller y guardo el dia
							if (listIsEmpty(cliente->vehiculos) == 1) { printf("\nNo tiene Vehiculos"); getchar(); continue; }
							printf("\nIngrese el id del vehiculo que quiere ingresar al taller: ");
							scanf("%d", &opId); getchar();

							vehiculo = listaVehiculoGetN(cliente->vehiculos, opId);
							
							if (vehiculo == NULL) continue;//no encontro el vehiculo

							printf("\n\n------>Ingresar vehiculo a Taller------\n");
							printf(" Fecha de ingreso de vehiculo al taller\n");
							printf("\n Dia: ");
							scanf("%s", &diaSemana); getchar();
							//si el estado = 0  no es comprado en la concesionaria y necesita estos datos extras
							if (vehiculo->estado!=1){
								printf(" Kilometraje: ");
								scanf("%d", &km); getchar();
								printf(" Costo: ");
								scanf("%f", &costo); getchar();
							}
							vehiculo->nMantenimientos++;
							m = mantenimientoNew(vehiculo, diaSemana, costo);

							mantenimientoPrint(m);
							//consigo la prioridad del vehiculo de acuerdo a su tipovehiculo
							if (strcmp(vehiculo->tipovehiculo, "taxi") == 0){ prio = 0; }
							else if (strcmp(vehiculo->tipovehiculo, "gama_alta") == 0){ prio = 1; }
							else if (strcmp(vehiculo->tipovehiculo, "gama_media") == 0){ prio = 2; }
							else if (strcmp(vehiculo->tipovehiculo, "gama_baja") == 0){ prio = 3; }

							tallerIngresar(taller, m, prio);
							printf("\nMantenimiento ingresado.."); getchar();
							break;
						case 2:// Atender auto 
							limpiar();
							tallerImprimir(taller, mantenimientoPrint);
							printf("\nPresiones enter para anter un vehiculo..."); getchar();
							nodo = tallerAtender(taller);
							if (nodo == NULL) continue;

							listAddNode(atendidos, nodo);// crear un List *atendidos
							
							limpiar();
							tallerImprimir(taller, mantenimientoPrint);
							printf("\nVehiculo atendido: ");
							m = nodeListGetCont(nodo);
							vehiculoPrint(m->vehiculo);
							getchar();
							break;
						case 4://imprimir taller
							limpiar();
							tallerImprimir(taller, mantenimientoPrint);//callback
							getchar();
						}
					} while (op3 != 3);
					break;
					//case 5: salir del submenu vehiculos ya lo controla el while
				}
			} while (opcion != 5);
			break;
		case 4://Generar Reportes
			op2 = 0;
			do{
				limpiar();
				printf("\n\tGenerar Peportes\n\n");
				printf("\n 1. Listado de vehiculos antendidos día\n");
				printf("\n 2. Listado de vehiculos antendidos en la semana\n");
				printf("\n 3. Listado de vehiculos de un Cliente\n");
				printf("\n 4. Mantenimientos de Vehiculos\n");
				printf("\n 5. Atras\n");
				printf("\n Ingrese una opcion: ");
				scanf("%d", &op2); getchar();
				switch (op2){
				case 1:
					printf("\n\n Ingrese dia: "); scanf("%s", &diaSemana); getchar();
					reporteVehiculos(atendidos, cliente, diaSemana, 1);
					break;
				case 2:
					reporteVehiculos(atendidos, cliente, diaSemana, 2);
					break;
				case 3://vehiculos del cliente
					flag = 0;
					concesionarioListarCli(concesionario);
					printf("\n\n Ingrese identificacion del cliente: ");
					scanf("%s", identificacion); getchar();
					for (p = concesionario->header; p != NULL; p = p->next){
						cliente = nodeListGetCont(p);
						if (strcmp(cliente->identificacion, identificacion) == 0){
							reporteVehiculos(cliente->vehiculos, cliente, identificacion, 3);
							flag = 1;
							break;
						}
					}
					if (flag == 0) printf("\nCliente no encontrado.."); getchar();
					
					break;
				case 4://reporte mantenimientos de vehiculos
					do{
						limpiar();
						printf("\n\tGenerar Peportes\n\n");
						printf("\n 1. Lista de Mantenimientos\n");
						printf("\n 2. Mantenimientos Realizados\n");
						printf("\n 3. Mantenimientos Pendientes\n");
						printf("\n 4. Atras\n");
						printf("\n Ingrese una opcion: ");
						scanf("%d", &op2); getchar();
						switch (op2)
						{
						case 1://atendidos y pendientes
							reporteMantenimientos(atendidos, taller, 1);
							break;
						case 2://mantenimietos realizados
							reporteMantenimientos(atendidos, taller, 2);
							break;
						case 3://mantenimienots pendientes
							reporteMantenimientos(atendidos, taller, 3);
							break;
						}
					} while (op2 != 4);//control sel submenu reporte mantenimientos

					break;
				}
			} while (op2 != 5);
			
			break;
		}
	} while (op1 != 5);
	
}

//IMPLEMENTACIONES
void menuPrincipal(){
	//limpiar();
	printf("\n-------Menu Principal-------\n");
	printf("1. Ambiente\n");
	printf("2. Clientes\n");
	printf("3. Vehiculos\n");
	printf("4. Generar reportes\n");	
	printf("5. Salir\n");
	printf("   Escoja una opcion: ");
}

void submenu(char *cadena){
	printf("\n------>Menu %s------\n", cadena);
	printf("1. Ingresar\n2. Modificar\n3. Eliminar\n");
	if (strcmp(cadena, "Clientes") == 0) printf("4. Listar Clientes\n");
	else printf("4. Taller\n");
	printf("5. ATRAS\n   Escoja una opcion: ");
}
void limpiar(){
	system("cls");
}

void reporteMantenimientos(List *atendidos, Taller *taller, int op){
	FILE *reporteM = NULL;
	reporteM = fopen("Reportes/reporteM.html", "w");
	NodeList *q, *pq;
	Vehiculo *vehi;
	Mantenimiento *m;
	List *lista;
	Taller *taller_aux = tallerNew(NP);
	Queue *Q;

	float cont = 0;
	fprintf(reporteM, "<!DOCTYPE html>\n");
	fprintf(reporteM, "<html>\n");
	fprintf(reporteM, "<head>\n");

	fprintf(reporteM, "<title> Reportes del Mantenimientos </title> \n");

	fprintf(reporteM, "<link rel=\"stylesheet\" href=\"reporteC.css\" type=\"text/css\"/>\n");//aqui va el header
	fprintf(reporteM, "</head>\n");
	fprintf(reporteM, "<body>\n\n");

	fprintf(reporteM, "<header> <img src=\"header.png\" alt=\"Banner\" style=\" width:1300px; height:150px; \"> </header>\n");

	fprintf(reporteM, "<section>\n\n");
	if (op == 1)
		fprintf(reporteM, "<h1> <font color=\"green\"> Mantenimientos Atendidos y Pendientes </font></h1>\n");
	else if (op == 2)
		fprintf(reporteM, "<h1> <font color=\"green\"> Mantenimientos Realizados </font></h1>\n");
	else if (op == 3)
		fprintf(reporteM, "<h1> <font color=\"green\"> Mantenimientos Pendientes  </font></h1>\n");

	fprintf(reporteM, "<table border=\"1\" style=\"width:50%% \"> \n");
	fprintf(reporteM, "<tr> \n");
	fprintf(reporteM, "<th><font color=\"green\">Marca</font></th>\n");
	fprintf(reporteM, "<th><font color=\"green\">Modelo</font></th>\n");
	fprintf(reporteM, "<th><font color=\"green\">Fecha de Fabricacion</font></th>\n");
	fprintf(reporteM, "<th><font color=\"green\">Tipo</font></th>\n");
	fprintf(reporteM, "<th><font color=\"green\">Estado</font></th>\n");
	fprintf(reporteM, "<th><font color=\"green\">Mantenimientos</font></th>\n");
	fprintf(reporteM, "<th><font color=\"green\">Costo</font></th>\n");
	fprintf(reporteM, "<th><font color=\"green\">Dia de Atencion</font></th>\n");
	if (op == 1)
		fprintf(reporteM, "<th><font color=\"green\">Estado de Atencion</font></th>\n");
	fprintf(reporteM, "</tr> \n");

	if (op == 1){
		for (q = atendidos->header; q != NULL; q = q->next){
			m = nodeListGetCont(q);
			vehi = m->vehiculo;
			fprintf(reporteM, "<tr>\n");
			fprintf(reporteM, "<td>%s</td>\n", vehi->marca);
			fprintf(reporteM, "<td>%s</td>\n", vehi->modelo);
			fprintf(reporteM, "<td>%s</td>\n", vehi->fecha_fabrica);
			fprintf(reporteM, "<td>%s</td>\n", vehi->tipovehiculo);
			fprintf(reporteM, "<td>%d</td>\n", vehi->estado);
			fprintf(reporteM, "<td>%d</td>\n", vehi->nMantenimientos);
			fprintf(reporteM, "<td>%.2f</td>\n", m->costo);
			fprintf(reporteM, "<td>%s</td>\n", m->diaDeSemana);
			fprintf(reporteM, "<td>Atendido</td>\n");
			fprintf(reporteM, "</tr>\n");
		}
		for (q = taller->header; q != NULL; q = q->next){
			Q = nodeListGetCont(q);//
			if (queueIsEmpty(Q) == 1)
				continue;
			for (pq = Q->header; pq != NULL; pq = pq->next){
				m = nodeListGetCont(pq);
				vehi = m->vehiculo;
				fprintf(reporteM, "<tr>\n");
				fprintf(reporteM, "<td>%s</td>\n", vehi->marca);
				fprintf(reporteM, "<td>%s</td>\n", vehi->modelo);
				fprintf(reporteM, "<td>%s</td>\n", vehi->fecha_fabrica);
				fprintf(reporteM, "<td>%s</td>\n", vehi->tipovehiculo);
				fprintf(reporteM, "<td>%d</td>\n", vehi->estado);
				fprintf(reporteM, "<td>%d</td>\n", vehi->nMantenimientos);
				fprintf(reporteM, "<td>%.2f</td>\n", m->costo);
				fprintf(reporteM, "<td>%s</td>\n", m->diaDeSemana);
				fprintf(reporteM, "<td>Pendiente</td>\n");
				fprintf(reporteM, "</tr>\n");
			}
		}
	}
	else if (op == 2){
		for (q = atendidos->header; q != NULL; q = q->next){
			m = nodeListGetCont(q);
			cont += m->costo;
			vehi = m->vehiculo;
			fprintf(reporteM, "<tr>\n");
			fprintf(reporteM, "<td>%s</td>\n", vehi->marca);
			fprintf(reporteM, "<td>%s</td>\n", vehi->modelo);
			fprintf(reporteM, "<td>%s</td>\n", vehi->fecha_fabrica);
			fprintf(reporteM, "<td>%s</td>\n", vehi->tipovehiculo);
			fprintf(reporteM, "<td>%d</td>\n", vehi->estado);
			fprintf(reporteM, "<td>%d</td>\n", vehi->nMantenimientos);
			fprintf(reporteM, "<td>%.2f</td>\n", m->costo);
			fprintf(reporteM, "<td>%s</td>\n", m->diaDeSemana);
			fprintf(reporteM, "</tr>\n");
		}
	}
	else if (op == 3){
		for (q = taller->header; q != NULL; q = q->next){
			Q = nodeListGetCont(q);//
			if (queueIsEmpty(Q) == 1)
				continue;
			for (pq = Q->header; pq != NULL; pq = pq->next){
				m = nodeListGetCont(pq);
				vehi = m->vehiculo;
				fprintf(reporteM, "<tr>\n");
				fprintf(reporteM, "<td>%s</td>\n", vehi->marca);
				fprintf(reporteM, "<td>%s</td>\n", vehi->modelo);
				fprintf(reporteM, "<td>%s</td>\n", vehi->fecha_fabrica);
				fprintf(reporteM, "<td>%s</td>\n", vehi->tipovehiculo);
				fprintf(reporteM, "<td>%d</td>\n", vehi->estado);
				fprintf(reporteM, "<td>%d</td>\n", vehi->nMantenimientos);
				fprintf(reporteM, "<td>%.2f</td>\n", m->costo);
				fprintf(reporteM, "<td>%s</td>\n", m->diaDeSemana);
				fprintf(reporteM, "</tr>\n");
			}
		}
	}

	if (op == 2)
		fprintf(reporteM, "<h3><font color=\"green\">Total de Costos:  $ %.2f </font> </h3>\n", cont);

	fprintf(reporteM, "</table> \n");
	fprintf(reporteM, "</section>\n\n");

	fprintf(reporteM, "<footer> @Copyright Estructuras 2015</footer> \n");

	fprintf(reporteM, "\n</body> \n");
	fprintf(reporteM, "</html> \n");
	fclose(reporteM);
	system("start Reportes/reporteM.html");
	return;
}

void reporteVehiculos(List *lista_vehiculos, Cliente *clie, char *cadena, int op){
	FILE *reporteC = NULL;
	reporteC = fopen("Reportes/reporteC.html", "w");
	NodeList *q;
	Vehiculo *vehi;
	Mantenimiento *m;
	fprintf(reporteC, "<!DOCTYPE html>\n");
	fprintf(reporteC, "<html>\n");
	fprintf(reporteC, "<head>\n");

	fprintf(reporteC, "<title> Reportes del Concecionario </title> \n");

	fprintf(reporteC, "<link rel=\"stylesheet\" href=\"reporteC.css\" type=\"text/css\"/>\n");//aqui va el header
	fprintf(reporteC, "</head>\n");
	fprintf(reporteC, "<body>\n\n");

	fprintf(reporteC, "<header> <img src=\"header.png\" alt=\"Banner\" style=\" width:1300px; height:150px; \"> </header>\n");

	fprintf(reporteC, "<section>\n\n");
	if (op == 1)
		fprintf(reporteC, "<h1> <font color=\"green\"> Vehiculos del Dia %s </font></h1>\n", cadena);
	else if (op == 2)
		fprintf(reporteC, "<h1> <font color=\"green\"> Vehiculos de la Semana </font></h1>\n");
	else if (op == 3)
		fprintf(reporteC, "<h1> <font color=\"green\"> Vehiculos del Cliente %s %s </font></h1>\n", clie->nombre, clie->apellido);

	fprintf(reporteC, "<table border=\"1\" style=\"width:50%% \"> \n");
	fprintf(reporteC, "<tr> \n");
	fprintf(reporteC, "<th><font color=\"green\">Marca</font></th>\n");
	fprintf(reporteC, "<th><font color=\"green\">Modelo</font></th>\n");
	fprintf(reporteC, "<th><font color=\"green\">Fecha de Fabricacion</font></th>\n");
	fprintf(reporteC, "<th><font color=\"green\">Tipo</font></th>\n");
	fprintf(reporteC, "<th><font color=\"green\">Estado</font></th>\n");
	fprintf(reporteC, "<th><font color=\"green\">Mantenimientos</font></th>\n");
	if (op != 3)
		fprintf(reporteC, "<th><font color=\"green\">Dia de Atencion</font></th>\n");
	fprintf(reporteC, "</tr> \n");
	for (q = lista_vehiculos->header; q != NULL; q = q->next){
		if (op == 1){
			m = nodeListGetCont(q);
			if (strcmp(m->diaDeSemana, cadena) == 0)
				vehi = m->vehiculo;
			else
				continue;
		}
		else if (op == 2){
			m = nodeListGetCont(q);
			vehi = m->vehiculo;
		}
		else if (op == 3){
			vehi = nodeListGetCont(q);
		}

		fprintf(reporteC, "<tr>\n");
		fprintf(reporteC, "<td>%s</td>\n", vehi->marca);
		fprintf(reporteC, "<td>%s</td>\n", vehi->modelo);
		fprintf(reporteC, "<td>%s</td>\n", vehi->fecha_fabrica);
		fprintf(reporteC, "<td>%s</td>\n", vehi->tipovehiculo);
		fprintf(reporteC, "<td>%d</td>\n", vehi->estado);
		fprintf(reporteC, "<td>%d</td>\n", vehi->nMantenimientos);
		if (op != 3)
			fprintf(reporteC, "<td>%s</td>\n", m->diaDeSemana);
		fprintf(reporteC, "</tr>\n");
	}
	fprintf(reporteC, "</table> \n");
	fprintf(reporteC, "</section>\n\n");

	fprintf(reporteC, "<footer> @Copyright Estructuras 2015</footer> \n");

	fprintf(reporteC, "\n</body> \n");
	fprintf(reporteC, "</html> \n");
	fclose(reporteC);
	system("start Reportes/reporteC.html");
	return;
}

