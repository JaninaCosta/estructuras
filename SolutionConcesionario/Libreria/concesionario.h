﻿#ifndef CONCESIONARIO_H
#define CONCESIONARIO_H

/*
* ARCHIVO:
*      concesionario.h
* 		�ltima modificaci�n: 2015/07/01 (B. CHITTO)
* --------------------------------------------------------------------------------------
* DESCRIPCION:
* 		Este archivo contiene la interfaz del TDA CONCESIONARIO (Concesionario)
* DEPENDENCIAS:
*		List.h Definici�n de TDA List, ya que Concesionario es una lista
*		Cliente.h Definici�n de TDA Cliente, ya que Concesionario guarda Clientes en sus nodos
* MODIFICACIONES:
*		2015/06/25 (B Bk):	Version original que define a concesionario como una lista
*		2015/07/01 (B.CH):	Version con nombres en español de los comportamientos del TDA
*/

#include "../TdaLib/list.h"
#include "Cliente.h"


typedef List Concesionario;


/*
* Funcion: Concesionario_New
* --------------------------
* Modo de uso:
* Concesionario *C;
* C = concesionarioNew()
* Esta operacion crea un sytema concesionario
* que es una lista de clientes
*/
Concesionario *concesionarioNew();

/*
* Funcion: concesionarioAgregarCli
* --------------------------
* Modo de uso:
* Concecionario *C;
* Cliente *cli;
* concesionarioAgregarCli(C, cli)
* Esta operacion recibe un Concesionario y aumenta al Cliente nuevo
* al final de la liesta de clientes
*/
void concesionarioAgregarCli(Concesionario *concesionario, Cliente *cliente);

/*
* Funcion: concesionarioEliminarCli
* --------------------------
* Modo de uso:
* Concecionario *C;
* Cliente *cli;
* concesionarioElimiarCli(C, cli)
* Esta operacion recibe un Concesionario y elimina un Cliente
* el cliente es identificado con el parametro *identifi
*/
int concesionarioEliminarCli(Concesionario *concesionario, char *identif);

/*
* Funcion: concesionarioModiricarCli
* --------------------------
* Modo de uso:
* Concecionario *C;
* Cliente *cli;
* concesionarioModificarCli(C, identificacion)
* Esta operacion recibe un Concesionario y modifica un Cliente
* el cliente es identificado con el parametro *identifi
*/
int concesionarioModificarCli(Concesionario *concesionario, char *identif);

/*
* Funcion: concesionarioListarCli
* --------------------------
* Modo de uso:
* Concecionario *C;
* concesionarioListarCli(C)
* Esta operacion recibe un Concesionario y presenta la lista Clientes
* en la consola
*/
void concesionarioListarCli(Concesionario *concesionario);


void generarClientesVehiculos(FILE *fclies, FILE *fvehis, Concesionario *conc);


#endif
