#ifndef _CLIENTE_H
#define	_CLIENTE_H

#include <stdio.h>
#include "listaVehiculos.h" //este incluye a List.h y a vehiculo.h

/*Estructuras de Cliente que guarda el nombre, apellido, identificacion del cliente*/
typedef struct TCliente{
	char nombre[20];
	char apellido[20];
	char identificacion[10];
	listaVehiculos *vehiculos;
}Cliente;

/*
* Funcion: clienteNew
* --------------------------
* Modo de uso:
* char *nombre;
* char *apellido;
* char*identificacion;
* C = clienteNew()
* Esta operacion crea un nuevo cliente
* que es una lista de clientes
*/
Cliente *clienteNew(char *nombre, char *apellido, char*identificacion);

/*
* Funcion: clienteModificar
* --------------------------
* Modo de uso:
* char *nombre;
* Cliente *cliente;
* char *apellido;
* char *identificacion;
* concesionarioModificarCli(C, identificacion)
* Esta operacion recibe un cliente y modifica un Cliente
* el cliente es identificado con el parametro *identifi
*/
void clienteModificar(Cliente *cliente, char *nombre, char *apellido, char*identificacion);
/*callback para imprimir un cliente*/
void printCliente(Cliente *cliente); //ListPrint




/** @} */
#ifdef	__cplusplus
extern "C" {
#endif


#ifdef	__cplusplus
}
#endif

#endif	