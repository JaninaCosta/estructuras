
#include "cliente.h"

Cliente *clienteNew( char *nombre, char *apellido, char *identificacion){
	Cliente *unCliente = (Cliente*)malloc(sizeof(Cliente));
	strcpy(unCliente->nombre, nombre);
	strcpy(unCliente->apellido, apellido);
	strcpy(unCliente->identificacion, identificacion);
	unCliente->vehiculos = listaVehiculosCrear();
	return unCliente;

}
void clienteModificar(Cliente *cliente, char *nombre, char *apellido, char*identificacion){
	strcpy(cliente->nombre, nombre);
	strcpy(cliente->apellido, apellido);
	strcpy(cliente->identificacion, identificacion);
}


void printCliente(Cliente *cliente){ //callback
	printf("%s\t", cliente->nombre);
	printf("%s\t", cliente->apellido);
	printf("%s\n", cliente->identificacion);

}

