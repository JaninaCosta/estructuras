#include "listaVehiculos.h"

listaVehiculos *listaVehiculosCrear(){
	return (listNew());
}
int listaVehiculosModificar(listaVehiculos *listaVehiculos, int idvehiculo){
	char marca[20];
	char modelo[20];
	char fecha_fabrica[20];
	char tipo[20];
	int estado;
	char tipovehiculo[20];
	int  opcionTipo = 0;
	NodeList *p = NULL;
	Vehiculo *vehiculo=NULL;
	for (p = listaVehiculos->header; p != NULL; p = p->next){
		vehiculo = (Vehiculo*)p->cont;
		if (vehiculo->idvehiculo==idvehiculo){
			printf("Ingrese nuevos datos\n");
			printf("idvehiculo:");
			scanf("%d", &idvehiculo);getchar();
			printf("marca:");
			scanf("%s", &marca); getchar();
			printf("modelo:");
			scanf("%s", &modelo); getchar();
			printf("fecha_fabrica:");
			scanf("%s", &fecha_fabrica); getchar();
			
			printf("tipo: ");
			printf("\n\t1. Taxi\n");
			printf("\t2. Gama Alta\n");
			printf("\t3. Gama Media\n");
			printf("\t4. Gama Baja\n");
			printf("\nEscoja el tipo de vehiculo\n");
			scanf("%d", &opcionTipo); getchar();
			setTipo(opcionTipo, tipovehiculo);			
			
			printf("estado:");
			scanf("%d", &estado); getchar();		
			vehiculoModificar(vehiculo, idvehiculo, marca, modelo, fecha_fabrica, tipovehiculo, estado);
			return 1; //si encuentra el vehiculo 
		}
		printf("\nVehicuo no encontrado\n");
		return 0; //si no encuentra el vehiculo 
	}
}

int listaVehiculosEliminar(listaVehiculos *listaVehiculos, int idvehiculo){
	NodeList *p=NULL;
	Vehiculo *vehiculo=NULL;
	for (p = listaVehiculos->header; p != NULL; p = p->next){
		vehiculo = (Vehiculo*)p->cont;
		if (vehiculo->idvehiculo == idvehiculo){
			listRemoveNode(listaVehiculos, p);
			return 1;
		}
	}
	printf("\nVehiculo no econtrado.. ");
	return 0;
}

void listaVehiculosAgregar(listaVehiculos *listaVehiculos, Vehiculo *vehiculo){//no retorno porque es un pntero
	NodeList *node;
	node = nodeListNew(vehiculo);
	listAddNode(listaVehiculos, node);
}

void listaVehiculosListar(listaVehiculos *vehiculos, char *nombre){
	NodeList *p;
	Vehiculo *vehi = NULL;
	int i = 1;
	printf("\n\t\tLista de vehiculos del Cliente %s\n", nombre);
	printf(" +---+--------------+----------+--------------+------------+------+--------+\n");
	printf(" | Id| Marca        | Modelo   |Fecha de Fabr.| Tipo       |Estado|Mantmtos|\n");
	printf(" +---+--------------+----------+--------------+------------+------+--------+\n");
	for (p = vehiculos->header; p != NULL; p = p->next){
		vehi = nodeListGetCont(p);
		printf(" |%2i | %-12s | %-9s|  %-12s| %-11s|%7i|%7i|\n",
			vehi->idvehiculo, vehi->marca, vehi->modelo, vehi->fecha_fabrica, vehi->tipovehiculo, vehi->estado, vehi->nMantenimientos);
	}
	printf(" +---+--------------+----------+--------------+------------+------+--------+\n");
	if (listIsEmpty(vehiculos) == 1){
		printf("\n\nNo tiene vehiculos.."); 
		getchar();
	}
}

Vehiculo *listaVehiculoGetN(listaVehiculos *vehiculos, int n){ //n: id del vehiculo que llega
	NodeList *p;
	Vehiculo *v;
	int indice = 0;
	for (p = vehiculos->header; p != NULL; p = p->next){
		v = nodeListGetCont(p);
		if (v->idvehiculo == n)
			return nodeListGetCont(p);
	}
	printf("\nIdVehiculo no encontrado..."); getchar();
	return NULL;
}



void setTipo(int opcionTipo, char *tipovehiculo){
	switch (opcionTipo)
	{
	case 1: strcpy(tipovehiculo, "taxi");
		break;
	case 2: strcpy(tipovehiculo, "gama_alta");
		break;
	case 3: strcpy(tipovehiculo, "gama_media");
		break;
	case 4: strcpy(tipovehiculo, "gama_baja");
		break;
	}
}
