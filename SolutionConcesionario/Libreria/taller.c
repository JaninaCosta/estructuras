
#include "taller.h"


/*Taller es un TDA cuya estructura interna es una cola de prioridad en donde cada nodo va
almacenado los vehiculos a ser atendidos junto con su costo y fecha(TDA <<Mantenimiento>>)*/
typedef List Taller;
Taller *tallerNew(int nprioridad){
	List *taller = listNew();
	Queue *Q;
	NodeList *node;
	for (int i = 0; i < nprioridad; i++){
		Q = queueNew();
		node = nodeListNew(Q);
		listAddNode(taller, node);
	}
	return taller;
}
void tallerIngresar(Taller *t, Generic gen, int npriori){
	NodeList *p;
	int i = 0; //la prioridad cero es del taxi
	for (p = t->header; p != NULL; p = p->next){
		if (i == npriori){ //pregunto si la prioridad que llega es de un TAXI
			queueEnqueue(p->cont, nodeListNew(gen)); //p->cont : cada contenido de la lista es una cola
			return;
		}
		i++; //cola FIFO los primeros en entrar son los taxis-GA-GM-GB
	}
	printf("\nPrioridad %i invalidad", npriori); getchar();
}
NodeList *tallerAtender(Taller *t){
	NodeList *p, *q;
	Queue *Q;
	int i = 0;
	for (p = t->header; p != NULL; p = p->next){
		Q = nodeListGetCont(p);
		if (queueIsEmpty(Q) != 1){//si la priodidad tiene elementos
			q = queueDequeue(Q);
			return q;
		}
	}
	printf("\nNo hay elementos que desencolar.."); //si no hay nigun elemento en niguna priodidad
	getchar();
	return NULL;
}
void tallerImprimir(Taller *t, printfn prin){
	NodeList *p, *q;
	Queue *Q;
	int i = 0, j = 0;
	printf("\n-Taller-\n\n");
	for (p = t->header; p != NULL; p = p->next){
		Q = nodeListGetCont(p);
		if (i == 0 ) printf("\t TAXIS. \n", i++);
		else if (i == 1) printf("\t GAMA ALTA. \n", i++);
		else if (i == 2) printf("\t GAMA MEDIA. \n", i++);
		else if (i == 3) printf("\t GAMA BAJA. \n", i++);
		for (q = Q->header; q != NULL; q = q->next){
			prin(q->cont);
		}
		printf("\n\n");
	}
}


void generarTallerAtendidos(FILE *fmant, Taller *taller, List *atendidos){
	Mantenimiento *mant;
	Vehiculo *vehi;
	int id, estado, priori;
	float costo;
	char diaSemana[20], marca[20], mod[20], fab[20], tipo[20];
	for (int i = 0; i < 10; i++){
		fscanf(fmant, "%d ,%s ,%s ,%s ,%s ,%d ,%s ,%f", &id, marca, mod, fab, tipo, &estado, diaSemana, &costo);
		if (strcmp(tipo, "taxi") == 0){ priori = 0; }
		else if (strcmp(tipo, "gama_alta") == 0){ priori = 1; }
		else if (strcmp(tipo, "gama_media") == 0){ priori = 2; }
		else if (strcmp(tipo, "gama_baja") == 0){ priori = 3; }
		vehi = vehiculoNew(id, marca, mod, fab, tipo, estado);
		vehi->nMantenimientos++;
		mant = mantenimientoNew(vehi, diaSemana, costo);
		tallerIngresar(taller, mant, priori);
		listAddNode(atendidos, nodeListNew(mant));
	}
}

//----------------------------------------------------------------------------------------

Mantenimiento *mantenimientoNew(Vehiculo *vehi, char *diaDeSemana, float costo){
	Mantenimiento *m = (Mantenimiento *)malloc(sizeof(Mantenimiento));
	if (vehi->estado == 1){//comprado
		if ((vehi->nMantenimientos % 4) == 0) //mantenimiento multi de 4
			m->costo = 800;
		else
			m->costo = 200;
	}
	else //no es comprado en la concesionario y viene el costo por teclado
		m->costo = costo;
	strcpy(m->diaDeSemana, diaDeSemana);
	m->vehiculo = vehi; //verificar esto
	return m;
}

void mantenimientoPrint(Mantenimiento *mant){//callback
	printf("\nMantenimiento\nVehiculo: ");

	vehiculoPrint(mant->vehiculo);
	printf("\nCosto: %.2f", mant->costo);
	printf("\nFecha: %s\n", mant->diaDeSemana);
}



