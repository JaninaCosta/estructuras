#ifndef _VEHICULO_H
#define	_VEHICULO_H

#include <stdio.h>    
#include "../TdaLib/list.h"

//ESTRUCTURA VEHICULO idVehiculo//
//estado = 1 : si es comprado en la concesionario
//estado = 0 : no es comprado fuera de la concesionario

typedef struct Vehiculo{
	int idvehiculo;
	char marca[20];
	char modelo[20];
	char fecha_fabrica[20];
	char tipovehiculo[20];
	int estado;  //comprado en la concesionario
	int nMantenimientos;
	int kilometros_recorridos;
}Vehiculo;

//FUNCIONES VARIAS//
/*
* Funcion: vehiculoNew
* --------------------------
* Modo de uso:
* int idvehiculo;
* char *marca;
* char*identificacion;
* char *model
* char *fecha_fabrica
* char *tipovehiculo
* int estado
* C = vehiculoNew()
* Esta operacion crea un nuevo vehiculo
*/
Vehiculo *vehiculoNew(int idvehiculo, char *marca, char *modelo, char *fecha_fabrica, char *tipovehiculo, int estado);


Vehiculo *vehiculoModificar(Vehiculo *vehiculo, int idvehiculo, char *marca, char *modelo, char *fecha_fabrica, char *tipovehiculo, int estado);

void vehiculoPrint(Vehiculo *vehiculo); //ListPrint
//void vehiculo_addMantenimiento(Vehiculo *vehiculo, char *mantenimiento);

/** @} */
#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif	