#ifndef _LISTAVEHICULOS_H
#define	_LISTAVEHICULOS_H

#include <stdio.h>
#include "vehiculo.h"
#include "../TdaLib/list.h"

//creo una listaVehiculos para agregar un vehiculo creado 
typedef List listaVehiculos;

//crea la lista de vehiculos
listaVehiculos *listaVehiculosCrear();

//listaVehiculosModificar: recibe la lista de vehiculos busco un vehiculo por medio de su idvehiculo
//si lo encuentra modifica todos sus valores y retorna 1 (si no lo encuentra retorna 0)
int listaVehiculosModificar(listaVehiculos *listaVehiculos, int idvehiculo);

//listaVehiculosEliminar: recibe lista de vehiculos y idvehiculo que me permite buscar un vehiculo por medio de su idvehiculo
//si lo encuentra en la lista lo elimina(retorna 1), retorna 0 si el vehiculo no se encuentra;
int listaVehiculosEliminar(listaVehiculos *listaVehiculos, int idvehiculo);

//listaVehiculosAgregar: recibe por parametro la lista de vehiculos y un vehiculo lo agrega a la lista
void listaVehiculosAgregar(listaVehiculos *listaVehiculos, Vehiculo *vehiculo );

//presenta la lista de vehiculos que tiene el cliente '*nombre'
void listaVehiculosListar(listaVehiculos *vehiculos, char *nombre);

Vehiculo *listaVehiculoGetN(listaVehiculos *vehiculos, int n);

void setTipo(int opcionTipo, char *tipovehiculo);



/** @} */
#ifdef	__cplusplus
extern "C" {
#endif


#ifdef	__cplusplus
}
#endif

#endif	