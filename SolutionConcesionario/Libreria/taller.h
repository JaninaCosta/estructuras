﻿
#ifndef TALLER_H
#define TALLER_H

/*
* ARCHIVO:
*      TALLER.h
* 		�ltima modificaci�n: 2015/07/01 (B. CHITTO)
* --------------------------------------------------------------------------------------
* DESCRIPCION:
* 		Este archivo contiene la interfaz del TDA TALLER (TALLER)
* DEPENDENCIAS:
*		List.h Definici�n de TDA List, ya que TALLER es una lista
*		Cliente.h Definici�n de TDA Cliente, ya que TALLER guarda Clientes en sus nodos
* MODIFICACIONES:
*		2015/06/25 (B Bk):	Version original que define a TALLER como una lista
*		2015/07/01 (B.CH):	Version con nombres en español de los comportamientos del TDA
*/

//LIBRERIAS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "../TdaLib/list.h"
#include "../TdaLib/queue.h"
#include "vehiculo.h"

typedef List Taller;

/*Mantenimiento es un TDA que consta de un TDA <<vehiculo>> al que se le va a hacer el
mantenimiento ademas de su costo y la fecha, este TDA sera los que ira almacenando el
TDA <<Taller>> dentro de cada nodo de su estructura*/
typedef struct TMantenimiento{
	Vehiculo *vehiculo;
	char diaDeSemana[10];
	float costo;
}Mantenimiento;


Taller *tallerNew(int nprioridad);
void tallerIngresar(Taller *t, Generic gen, int npriori);
NodeList *tallerAtender(Taller *t);
void tallerImprimir(Taller *t, printfn prin);

//genera taller con matenimietos desde archivo
void generarTallerAtendidos(FILE *fmant, Taller *taller, List *atendidos);

Mantenimiento *mantenimientoNew(Vehiculo *vehi, char *diaDeSemana, float costo);

void mantenimientoPrint(Mantenimiento *mant);

#endif
