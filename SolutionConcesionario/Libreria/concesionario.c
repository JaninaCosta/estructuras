﻿
#include "concesionario.h"

//comportamientos de TConcesionario
Concesionario *concesionarioNew(){
	return (listNew());
}
void concesionarioAgregarCli(Concesionario *concesionario, Cliente *cliente){
	NodeList *node = NULL;
	node = nodeListNew(cliente);
	listAddNode(concesionario, node);
}
int concesionarioEliminarCli(Concesionario *concesionario, char *identif){
	NodeList *p;//
	Cliente *cliente;
	for (p = concesionario->header; p != NULL; p = p->next){
		cliente = nodeListGetCont(p);
		if (strcmp(cliente->identificacion, identif) == 0){
			listRemoveNode(concesionario, p);
			return 1;
		}
	}
	printf("\nCliente no econtrado.. ");
	return 0;
}
int concesionarioModificarCli(Concesionario *concesionario, char *identif){
	char nombre[30], apellido[30], identificacion[30];
	NodeList *p;
	Cliente *cliente;
	for (p = concesionario->header; p != NULL; p = p->next){
		cliente = nodeListGetCont(p);
		if (strcmp(cliente->identificacion, identif) == 0){

			printf("\n\n------Ingrese Nuevos Datos------\n");
			printf("\nNombre: ");
			scanf("%s", &nombre); getchar();
			printf("\nApellido: ");
			scanf("%s", &apellido); getchar();
			printf("\nIdentificacion: ");
			scanf("%s", &identificacion); getchar();

			clienteModificar(p->cont, nombre, apellido, identificacion);
			return 1;
		}
	}
	printf("\nCliente no econtrado.. ");
	return 0;
}

void concesionarioListarCli(Concesionario *concesionario){
	NodeList *p;
	Cliente *clie = NULL;
	int n_vehis = 0;
	printf("\n         Lista de Clientes del Concesionario\n");
	printf(" +----------------+---------------+----------------+------------+\n");
	printf(" |  Nombre        | Apellido      | Identificacion |N. Vehiculos|\n");
	printf(" +----------------+---------------+----------------+------------+\n");
	for (p = concesionario->header; p != NULL; p = p->next){
		clie = p->cont;
		n_vehis = listGetSize(clie->vehiculos);
		printf(" |  %-14s| %-14s| %9s      | %10d |\n", clie->nombre, clie->apellido, clie->identificacion, n_vehis);
	}
	printf(" +----------------+---------------+----------------+------------+\n");
}


void generarClientesVehiculos(FILE *fclies, FILE *fvehis, Concesionario *conc){
	NodeList *nodo, *p;
	Cliente *clie;
	Vehiculo *vehi;
	int id, estado;
	char nombre[20], apellido[20], identificacion[20], marca[20], mod[20], fab[20], tipo[20];
	do{
		fscanf(fclies, "%s ,%s ,%s\n", nombre, apellido, identificacion);
		clie = clienteNew(nombre, apellido, identificacion);
		concesionarioAgregarCli(conc, clie);

	} while (!feof(fclies));

	for (p = conc->header; p != NULL; p = p->next){
		clie = nodeListGetCont(p);
		do{
			fscanf(fvehis, "%d ,%s ,%s ,%s ,%s ,%d", &id, marca, mod, fab, tipo, &estado);
			if ((int)rand() % 2 == 1){//agrega o no aleatoriamente el vehiculo de una fila
				vehi = vehiculoNew(id, marca, mod, fab, tipo, estado);
				vehi->nMantenimientos = 2;//porque ya hay por defecto autos en la lista atendidos
				listaVehiculosAgregar(clie->vehiculos, vehi);
			}
		} while (!feof(fvehis));
		rewind(fvehis);
	}

}